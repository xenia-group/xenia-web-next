FROM node:18 AS base
RUN npm i -g pnpm

FROM base AS dependencies

ENV PORT 4321
EXPOSE 4321

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json pnpm-lock.yaml ./
RUN pnpm install
COPY . .
RUN scripts/install-sharp.sh

CMD ["pnpm", "dev", "--host"]

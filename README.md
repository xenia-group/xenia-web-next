# xenia-web-next

Repository for the new Xenia Project website made by [Mitsunee]

## Development Setup

This project uses [Astro] with [Preact] for interactive components.

Make sure you have [node.js] (version 22) and [pnpm] (version 9) installed. Install all dependencies and start the Development server:

```shell
pnpm install
pnpm dev # or pnpm build:types
```

There are git hooks in place to format all staged files prior to committing. Please run `pnpm lint` prior to committing if your editor does not integrate ESLint. Github Actions will check formatting, lint and check types on each push.

## Documentation

See [/docs](./docs) for documentation.

## Testing

These are the same commands also run in CI on every push:

```shell
pnpm lint:strict
pnpm format:check
pnpm build:types && pnpm typecheck
```

<details>
<summary>Full list of commands</summary>

```shell
pnpm format # formats entire codebase
pnpm format:check # checks formatting on entire codebase
pnpm lint # standard lint check on entire codebase
pnpm lint:fix # standard lint check on entire codebase with autofix enabled
pnpm lint:strict # strict lint check on entire codebase
pnpm dev # start dev server
pnpm typecheck # run typechecks on entire codebase
pnpm build:types # generates content/data collection types
pnpm build # create production build
```

</details>

### Docker

If you want to use Docker/Podman to test the website, you can use the provided `Dockerfile`:

```shell
docker build -t local/xenia-web-next .
docker run -p 4321:4321 local/xenia-web-next
```

You will then be able to access the dev server on `http://localhost:4321`.

## Production build

Simply run the build script:

```shell
pnpm build
```

To test a production build locally either use `pnpm astro preview` or [binserve](https://github.com/mufeedvh/binserve) with the provided configuration.

## Assets used

- Heroicons https://heroicons.com/ : UI Iconography
- Fontsource https://fontsource.org/ : Font packages for fonts Noto Sans, Jetbrains Mono and Carlito
- Xenia Linux branding :)

[mitsunee]: https://www.mitsunee.com
[astro]: https://docs.astro.build/en/getting-started/
[preact]: https://preactjs.com/guide/v10/getting-started
[node.js]: https://nodejs.org/en/
[pnpm]: https://pnpm.io/

// @ts-check
import mdx from "@astrojs/mdx";
import preact from "@astrojs/preact";
import { defineConfig } from "astro/config";
import unwrapImagesPlugin from "rehype-unwrap-images";

// https://astro.build/config
export default defineConfig({
  site: "https://xenialinux.com",
  outDir: "public",
  publicDir: "static",
  cacheDir: "./.astro-cache",
  markdown: {
    syntaxHighlight: "prism",
    rehypePlugins: [unwrapImagesPlugin],
    gfm: true
  },
  integrations: [preact(), mdx()],
  experimental: { contentIntellisense: true }
});

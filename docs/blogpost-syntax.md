# Blogpost Syntax

Blogposts are written in MDX. The syntax is based on Github-flavoured Markdown with the inclusion of syntax-highlighting via Prism and support for Framework Components via Astro. Post title, publish date and such are included as Frontmatter.

## Creating a new blogpost

Create a new `.mdx` file in `./src/content/blog/`. Sub-directories are supported and will be reflected in the URL. It is not recommended to move a blogpost that has been previously published as that will change the URL!

If you want to use images or a thumbnail create a folder in `./src/assets/blog/` with the same name as your blogpost file (not strictly required, just a good naming convention). You can then easily import your images from `"~/assets/blog/my-post-slug/`.

## Frontmatter Properites

Each Blogpost must have the following properties in Frontmatter:

```yml
title: Title of the blog post
description: A short description of the post's content
date: Publish Date in format YYYY-mm-DD HH:MM:ss Z # such as: 2023-08-13 21:00:00 +0100
editedDate: Update Date in format YYYY-mm-DD HH:MM:ss Z # optional
categories: slug # filename of file in ./src/content/category
author: slug # filename of file in ./src/content/author
```

You can also include the path to an image to include as the thumbnail:

```yml
image: ~/assets/blog/path-to-image/some-image.png
```

## Adding categories

To add a new category create a `.yml` file in `./src/content/category` with the following properties:

```yml
title: Title of the Category # should be title case or similar
description: Description of what type of content is in the category # should be single-line text
```

## Adding author

_Authors are not fully implemented yet_

To add a new author create a `.yml` file in `./src/content/authors` with the following properties:

```yml
name: Name of the author
fedi: Fediverse username of the author # optional
```

## Figure

The Figure component can be used to display an image with a description. The image should be imported from `"~/assets/"`. All images are also resized to the appropriate size for blog posts and converted to avif (with the original file extension as a fallback) by the `Picture` component. The figure also links to the original image, which can also be viewed by clicking on the image, opening the original in an overlay.

<!-- prettier-ignore -->
```jsx
import Figure from "~/components/Figure.astro";
import myImg from "~/assets/blog/my-post/my-image.png";

<Figure image={myImg} caption="Lorem ipsum dolor sit amet" />
```

Note that since this is using JSX syntax the must be a closing tag or slash present. The above example uses the closing slash `/>` since it has no child elements. Note that Prettier may reformat your element if it goes beyond a line length of 80 characters.

You can also include additional text to be included in the caption and/or hide the original caption with the `hideAlt` prop):

<!-- prettier-ignore -->
```jsx
import Figure from "~/components/Figure.astro";
import myImg from "~/assets/blog/my-post/my-image.png";

<Figure image={myImg} caption="Lorem ipsum dolor sit amet" hideAlt>
  Lorem ipsum **dolor** [sit amet](/other-page)
</Figure>
```

## Formatting

If you wrote your post in something other than VSCode (or fork) you may need to manually run the formatting command:

```shell
pnpm run format
```

Posts are auto-formatted when committing as well, but it is recommended to check for errors created by Prettier before publishing.

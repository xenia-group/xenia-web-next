# Code Comment Conventions

To make finding placeholders, bugs and such easier to find this repository uses tagged comments as a convention

## Usage

### JavaScript / TypeScript

```js
const _foo = "foobar"; // TODO: This is an example TODO item on this line

/** TODO:
 * This is an example TODO item for the following code section
 */
function _bar() {
  // code goes here :3
}
```

### YAML / MDX Frontmatter

```yml
title: Example
description: Placeholder Description # PLACEHOLDER: write description later
```

### JSX / TSX

```tsx
export function SomeComponent() {
  return (
    <ul>
      {/* PLACEHOLDER */}
      <li>Placeholder item</li>
    </ul>
  );
}
```

## `grep` command for finding tagged comments

`grep` can be used to find all tagged comments in a directory (usually `src`). Tags can be added/removed to the `|`-separated list to narrow down to specific labels. The directory can also be narrowed down, for example `src/content/blog` to only search for comments in blog posts.

```shell
grep -nrHEi '(/{2,}|/\*+|#+) ?(TODO|WIP|DEBUG|PLACEHOLDER|FIXME|TEMP|BUG)' src
```

## Common Tags

### `TODO`

The TODO tag should be used to mark missing functionality and/or notes for future features.

### `PLACEHOLDER`

Any placeholder texts or images should be marked with the `PLACEHOLDER` tag. Ideally any actually rendered items should also state their placeholder status, see examples above.

### `BUG` / `FIXME`

Code with known bugs or oversights should be marked with `BUG` or `FIXME`, ideally with a short description.

### `WIP`

Code that is currently being worked on and is likely incomplete can be marked with `WIP` tagged comments. This is not needed when it's already obvious such as in Blog Posts with `draft: true`

### `TEMP` / `DEBUG`

Temporary code, debug output or changes can be marked with `TEMP` or `DEBUG` respectively. This makes it obvious that the marked item will likely disappear or significantly change in future commits.

### Others

The convention is open to other labels, such as `NOTE`, as needed.

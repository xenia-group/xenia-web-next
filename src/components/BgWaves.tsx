import type { HTMLProps } from "~/types/jsx";

export function BgWaves(props: HTMLProps<"svg">) {
  return (
    <svg
      width={3840}
      height={2160}
      {...props}
      version="1.2"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 3840 2160">
      <path fill="#ff6982" d="m-424 2024l2876-236c0 0-3188-431.6-2876 236z" />
      <path fill="#ff5757" d="m1436 1856c0 0 2796-803.6 2496-172" />
      <path fill="#ff780b" d="m1236 1932c0 0 3236-771.6 2608-80" />
      <path fill="#ff9845" d="m204.4 2042.9c0 0 4273.5-777.6 3826.2 24.8" />
      <path fill="#ff5757" d="m220 1904l2696 196c0 0-2692-595.6-2696-196z" />
      <path fill="#ff780b" d="m-120 2140l4028 196c0 0-3736-1027.6-4028-196z" />
      <path fill="#ff9845" d="m-68 2196l3976-8c0 0-3324-607.6-3976 8z" />
      <path fill="#ffbe8a" d="m-80 2192c0 0 4132-643.6 3968-32" />
    </svg>
  );
}

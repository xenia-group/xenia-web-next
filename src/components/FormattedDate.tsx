import { useMemo } from "preact/hooks";
import spacetime from "spacetime";
import { dateFormats } from "~/types/dateFormats";
import { useIsClient } from "~/utils/useIsClient";

interface FormattedDateProps {
  fallback: string;
  datetime: string;
  format?: string;
}

export function FormattedDate({
  fallback,
  datetime,
  format
}: FormattedDateProps) {
  const isClient = useIsClient();
  const s = useMemo(() => spacetime(datetime, "utc").goto(null), [datetime]);
  return (
    <time dateTime={datetime}>
      {isClient ? s.format(format || dateFormats.full) : fallback}
    </time>
  );
}

import type { HTMLProps } from "~/types/jsx";

export function IconMinus(props: HTMLProps<"svg">) {
  return (
    <svg
      height="1rem"
      width="1rem"
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 20 20"
      fill="currentColor">
      <path
        fillRule="evenodd"
        d="M4 10a.75.75 0 01.75-.75h10.5a.75.75 0 010 1.5H4.75A.75.75 0 014 10z"
        clipRule="evenodd"
      />
    </svg>
  );
}

import type { HTMLProps } from "~/types/jsx";

export function IconXenia(props: HTMLProps<"svg">) {
  return (
    <svg
      height="1rem"
      width="1rem"
      {...props}
      version="1.2"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 5000 5000"
      fill="#ff780b">
      <title>{props.title || "Xenia logo"}</title>
      <path fillOpacity="0.5" d="m1939 3060l1209 198 121 291" />
      <path fillOpacity="0.5" d="m3549 4285l-269-670-835-330" />
      <path d="m3874 2191l-648 1038 414 1092" />
      <path fillOpacity="0.5" d="m3094 3211l-1008-318-180 126" />
      <path d="m3922 2227l-240 2082 390-876" />
      <path fill="#000000" d="" />
      <path d="m1894 2971l-174-258" />
      <path fillOpacity="0.5" d="m2002 2911l-126 78-180-258" />
      <path fillOpacity="0.5" d="m724 1975l726 564 540 360-1368-1122" />
      <path fillOpacity="0.5" d="m634 1549l-18 210 1386 1140" />
      <path fillOpacity="0.5" d="m1906 2299l102 582-924-936" />
      <path d="m580 1399l54 126" />
      <path fillOpacity="0.5" d="m1054 1939l-462-540 48 138" />
      <path fillOpacity="0.5" d="m1906 2281l-1284-870 444 499" />
      <path d="m640 1117l960 264-996-18" />
      <path fill="#000000" d="m616 1177l-102 108 72 156" />
      <path fill="#000000" d="m870 1983" />
      <path d="m1606 1405l294 828-1260-852z" />
      <path d="m1648 1363l768-150" />
      <path d="m1936 2197l-282-798 690-186z" />
      <path fillOpacity="0.8" d="m2050 2851l318-1614-414 990" />
      <path fillOpacity="0.8" d="m2428 1225l396-42-744 1644" />
      <path fillOpacity="0.8" d="m3142 1232l-288-36-756 1650" />
      <path d="m3274 1087l-126 108 198 54" />
      <path fillOpacity="0.8" d="m3172 1273l-1056 1590 1188-1542" />
      <path fillOpacity="0.8" d="m3358 1375l468 738-654 1050" />
      <path fillOpacity="0.8" d="m3388 1303l468-354 18 1110" />
      <path fillOpacity="0.8" d="m3334 1369l-1200 1500 984 294" />
      <path fillOpacity="0.9" d="m3917 906l307-137-318 1269" />
      <path fillOpacity="0.9" d="m4488 637l-549 1423 340-1307" />
      <path d="m4483 775l-511 1291 516-1110" />
    </svg>
  );
}

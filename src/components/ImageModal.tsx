import type { WithChildren } from "~/types/jsx";
import { useModal } from "~/utils/useModal";
import { IconClose } from "./IconClose";
import styles from "./ImageModal.module.css";

interface ImageModalProps extends WithChildren {
  src: string;
  width: number;
  height: number;
  alt: string;
}

export function ImageModal({
  children,
  src,
  width,
  height,
  alt
}: ImageModalProps) {
  const [dialog, createDialog, showDialog] = useModal();

  return (
    <a
      href={src}
      onClick={ev => {
        ev.preventDefault();
        showDialog();
      }}
      className={styles.link}>
      {children}
      {createDialog(
        <>
          <button
            className={styles.close}
            onClick={() => dialog?.close()}
            aria-label="Close">
            <IconClose height="1.25rem" width="1.25rem" />
          </button>
          <img
            src={src}
            alt={alt}
            width={width}
            height={height}
            className={styles.img}
          />
        </>
      )}
    </a>
  );
}

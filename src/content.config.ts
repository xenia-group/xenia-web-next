import { glob } from "astro/loaders";
import type { ImageFunction } from "astro:content";
import { defineCollection, reference, z } from "astro:content";

const CategorySchema = z.object({
  title: z.string({
    description:
      "Title of the category displayed in Post footer and article cards"
  }),
  description: z.string({
    description: "Description to display on the category's page"
  }),
  hidden: z
    .boolean({
      description: "Set true to hide category from public view (unused)"
    })
    .optional()
});

const blogCategoryCollection = defineCollection({
  loader: glob({ pattern: "**/*.yml", base: "./src/content/category" }),
  schema: CategorySchema
});

const AuthorSchema = z.object({
  name: z.string({ description: "Name of the author" }),
  fedi: z
    .string({
      description:
        "Fediverse username of the author such as '@user@fedi.example.com'. Value should be surrounded by quotes to have valid YAML syntax."
    })
    .transform((arg, ctx) => {
      const match = arg.match(/^@([^@]+)@([a-z0-9.]+\.[a-z]+)$/);
      if (!match) {
        ctx.addIssue({
          code: z.ZodIssueCode.custom,
          message:
            "Value must be valid fediverse username such as '@user@fedi.example.com'"
        });
        return z.NEVER;
      }

      return { user: match[1], instance: match[2] };
    })
    .optional()
});

const blogAuthorCollection = defineCollection({
  loader: glob({ pattern: "**/*.yml", base: "./src/content/authors" }),
  schema: AuthorSchema
});

const zDate = z
  .string({ description: "Date as string in format YYYY-mm-DD HH:MM:ss Z" })
  .regex(
    /^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} (\+|-)\d{4}$/,
    "Date must be in format YYYY-mm-DD HH:MM:ss Z"
  );

const BlogPostSchema = ({ image }: { image: ImageFunction }) =>
  z.object({
    title: z.string(),
    description: z.string(),
    date: zDate,
    editedDate: zDate.optional(),
    categories: reference("category"),
    author: reference("authors"),
    image: image().optional(),
    draft: z.boolean().optional()
  });

const blogPostCollection = defineCollection({
  loader: glob({ pattern: "**/*.mdx", base: "./src/content/blog" }),
  schema: BlogPostSchema
});

export type BlogCategory = z.infer<typeof CategorySchema>;
export type BlogAuthor = z.infer<typeof AuthorSchema>;
export type BlogPost = z.infer<ReturnType<typeof BlogPostSchema>>;

export const collections = {
  blog: blogPostCollection,
  category: blogCategoryCollection,
  authors: blogAuthorCollection
};

---
title: "Xenia Linux 0.4 'Velox' is now out!"
description: Xenia Linux 0.4 is out! Learn about the new installer, support for different filesystems, and experimental Portage support.
date: 2023-07-17 23:22:00 +0100
categories: release
author: luna
image: ~/assets/blog/xenia-linux-0-4-release/desktop.png
---

import Figure from "~/components/Figure.astro";
import desktopImg from "~/assets/blog/xenia-linux-0-4-release/desktop.png";
import packagingImg from "~/assets/blog/xenia-linux-0-4-release/packaging.png";

<Figure image={desktopImg} caption="A picture of the Xenia Linux desktop" />

Hi! Today we released Xenia Linux 0.4 'Velox'! Here is the TL;DR of what we added:

- A new installer
- Different filesystem support (btrfs, lvm and traditional filesystems), and the move the btrfs as default
- Set of system utilities such as a system logger and a cron daemon
- Experimental portage support

## I don't wanna read anymore! How do I update?

Follow the guide [here](https://gitlab.com/groups/xenia-group/-/wikis/usage/Update-your-system) to update an existing installation of Xenia Linux. Make sure you are already on 0.3! If you aren't - the easiest way to get 0.4 is to re-install.

## Why did you take so long?

Xenia Linux 0.4 has been a big release since the start, with the addition of multiple packages, the struggle to get multiple filesystems working and a whole new installer built from the ground-up in Python.

Initially, we was going to release 0.4 without the new installer (0.4 has been done for _weeks_ in the unstable branch), but the new installer is what ties this whole release together, mainly with the support for multiple filesystems. So, let's start with that!

## Features

### Installer

The old installer was _terrible_, there is no way around that. It just ran through a bash script and hoped that it worked! It only supported a pretty bad default filesystem layout with LVM, and needed to be manually edited to specify drives - not to mention it didn't even work on NVMe or eMMC drives!

To fix this, Cow started work on a new installer for Xenia, written from the ground-up in Python. Together, we made an installer that uses a TOML file to configure itself, as well as making an interactive mode for those without config.

This means an average user can use the installer now as is and be up and running through an interactive process, and advanced users can have a small configuration file which specifies the disk, the filesystem layout and the root URL.

We hope the new installer helps new users get a system up and running quickly with a nicer experience! You can follow the instructions [here](https://gitlab.com/groups/xenia-group/-/wikis/installation/guided-install) to use the new installer.

### Different filesystem layouts

With a rework of how we boot Xenia, and how we mount the overlays, Xenia can now detect and use Btrfs, LVM and traditional filesystems (ext4, XFS), with the plumbing in-place for ZFS and other filesystems in the future. With these filesystems being added, we also added options in the new installer to use Btrfs, LVM or a traditional layout.

The default in Xenia 0.4 have been changed from a legacy LVM layout to Btrfs on new installs. However, people upgrading from 0.3 to 0.4 have nothing to worry about! It will still work just fine.

With Btrfs, users can make use of features like snapshots while keeping all their drive space for what needs it, instead of segregating the disk into multiple logical volumes or partitions. We really like how Btrfs is working on Xenia and we hope to use it in the future to implement features like automatic `/usr` overlay snapshots.

### New system utilities

As mentioned in the [previous blog post](https://blog.xenialinux.com/preview/2023/06/21/xenia-linux-preview-rebrand.html#a-better-set-of-system-utilities), we have implemented a set of system utilities into Xenia:

- chrony for NTP services
- cronie for a cron daemon
- sysklogd for syslog
- mlocate to index files and provide the `locate` utility

We have also added QEMU guest agents and `spice-vdagent` for integration in KVM virtual machines.

> Note: For some reason, QXL graphics don't play nice with `spice-vdagent` in Xenia. Virtio graphics, including virgl, work just fine.

### Experimental Portage support

<Figure
  image={packagingImg}
  caption="A screenshot of Xenia Linux showing all 3 packaging options: A Firefox flatpak window, a Fedora distrobox and an emerge running for Tailscale"
/>

Also mentioned in the [previous blog post](https://blog.xenialinux.com/preview/2023/06/21/xenia-linux-preview-rebrand.html#usr-overlay-portage-support) was the implementation of a `/usr` overlay. A couple implementation details have changed since then:

- The `/usr` overlay is mounted by the initramfs along with the other overlays to allow for access at early boot
- The service in `/etc/local.d` now instead remounts the overlay as read-only, and can be changed to allow for it to be read-write on boot.

Portage support is still experimental for now! For one, updating your system with it is bound to lead to conflicts, and we don't recommend doing that. It is important to remember that Portage support is there for the times where you absolutely need it - for example, Tailscale, or proprietary Nvidia drivers.

And that about concludes all the new features.

## The Wiki

We have been slowly working on expanding the wiki! We now have articles on [manual and guided installs](https://gitlab.com/groups/xenia-group/-/wikis/installation), maintenance guides such as [updating your system](https://gitlab.com/groups/xenia-group/-/wikis/usage/Update-your-system) as well as development guides, for example to [build your own root image](https://gitlab.com/groups/xenia-group/-/wikis/development/rootfs/Manual-rootfs-generation).

The wiki is a core part of Xenia Linux, as without it the knowledge of the system is left to us - making it unfair for others who want to contribute or use parts of Xenia Linux in there own work. If you want to write a page, or edit one, let us know! And if you have any feedback at all, feel free to contact me or the rest of the team.

You can get contact information on the website by running `contact` in the terminal.

## What's next?

The biggest change yet to come is unfortunately an internal one - we need to move all of our infrastructure as well as revise all of our build tools to use Catalyst 4. This not only will give us some amount of delay, as Catalyst 4 isn't in stable yet, and Catalyst 3 is deprecated (yeah, I don't know either) - it doesn't actually give you any benefit. Sorry about that!

This also means those looking to make your own rootFS, for now I'm sorry to say you just _can't_ without getting the old version of catalyst (now masked) or by using catalyst-9999 and diverging from our own instructions.

In less depressing news, we are working on getting `gnome-initial-setup` ready for the next release (or the release after)! This will give a really nice experience out of the box (or after install), where you can setup your users, network and such. Thanks Mia for the work on this!

Lastly, we plan on making our Portage support more robust, with automatic snapshots of the `/usr` overlay and a custom @xenia set which you can use to install and update packages with, without trying to update the whole system through Portage (bad, bad idea. seriously).

## Conclusion

With the addition of new filesystem support, experimental Portage support, the new installer and a nicer base set of utilities, Xenia Linux 0.4 'Velox' is our biggest release yet.

Thank you to everyone who has helped on this release and provided us with support, whether that be helping make our docs better (thanks Duality), or starting the new installer (thanks Cow), or being on call with me nearly every day with support (cheers Jack) - we really appreciate it (or I suppose I do in the case of Jack - maybe he thinks I'm more of a burden lol).

We hope you try Xenia and fall in love with it like we did!

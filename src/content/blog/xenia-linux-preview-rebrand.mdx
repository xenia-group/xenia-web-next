---
title: "Xenia Linux now supports Portage (amongst other things), plus the whole rebrand thing"
description: Some new features are here! Learn about portage support, as well as the new boot logic which allows for new filesystem layouts to be supported.
date: 2023-06-22 00:18:45 +0100
categories: preview
author: luna
image: ~/assets/blog/xenia-linux-preview-rebrand/desktop.png
---

import Figure from "~/components/Figure.astro";
import desktopImg from "~/assets/blog/xenia-linux-preview-rebrand/desktop.png";
import websiteImg from "~/assets/blog/xenia-linux-preview-rebrand/website.png";
import bgsImg from "~/assets/blog/xenia-linux-preview-rebrand/bgs.png";
import tailscaleImg from "~/assets/blog/xenia-linux-preview-rebrand/tailscale.png";

<Figure
  image={desktopImg}
  caption="A picture of the Xenia Linux preview desktop"
/>

Hi again! I wanted to write today a bit about the state of Xenia Linux, upcoming features and talk about the rebrand that you may have noticed! First, lets start with the rebrand.

## Rebrand

Our old branding was a bit of a mess. We had a website that was thrown together in a couple hours, a logo we made from pre-existing artwork and not much else. We mainly made it just as a placeholder until we decided to make something properly.

With our [new branding](https://gitlab.com/xenia-group/branding), we strived to achieve a cleaner look while still keeping Xenia the fox as a main focus (it _is_ our name after all!).

The first change you may have noticed is the [website](https://xenialinux.com/). We made the webpage a terminal! With the help of jquery-terminal, our page features a terminal which shows our other pages through a "neofetch", as well as having some limited functionality to take you to other pages and show information. There is even a secret if you look hard enough (or look at the source code).

<Figure image={websiteImg} caption="A picture of the new website" />

We also have new default backgrounds! They aren't actually set as default yet, but they do show up in GNOME settings. We went with a simplistic wave design utilising our [new palletes](https://gitlab.com/xenia-group/branding/-/tree/main/pallete).

<Figure image={bgsImg} caption="A picture of the new backgrounds" />

You might also notice the new neofetch (and logo). I created the new logo with a side-profile of a fox, keeping the theme of Xenia the fox (but not using other people's art this time). If you want the new neofetch for yourself, clone `https://gitlab.com/xenia-group/neofetch` and install with `sudo make install`...

Wait a minute? Doesn't that try to install to `/usr`, meaning it won't be persistent? Well, I suppose it's time to take a look at the new features coming soon!

## New features

### /usr overlay (Portage support!)

It's official! Xenia Linux is now the _only_ immutable distro to support Portage. Starting from now in the unstable branch, and for release 0.4 which will be coming out soon, Xenia Linux implements an overlay over `/usr` (read-only by default, we'll talk about this later), giving you the ability to use Portage (and make other changes to `/usr/`).

<Figure
  image={tailscaleImg}
  caption="A picture of Tailscale running on Xenia"
/>

This is a big moment as it provides applications that were previously difficult or impossible to install (such as Tailscale, libvirt and more) on Xenia. We haven't done much testing at all beyond installing applications such as Tailscale (a personal usecase for me), so the `/usr` overlay is read-only by default.

To enable the overlay as read-write temporarily, perform the following:

```
xenia ~ # mount -o rw,remount /usr
```

To make the overlay read-only again, either reboot or type the following:

```
xenia ~ # mount -o ro,remount /usr
```

If you would like to try the overlay without updating to unstable, copy the service [here](https://gitlab.com/xenia-group/catalyst/-/raw/unstable/overlay/etc/local.d/usr.start) to your `/etc/local.d/` and mark it executable.

When using portage, make sure to edit your `make.conf` and add your `MAKEOPTS`! You don't want to be stuck on one core for compilation.

We have not tested this functionality thoroughly, please be _cautious_. Also, please adhere to the following guidelines:

- Do not update system software with portage! This will cause conflicts to arise when you update your `root.img`.
- Refrain from installing software that can be easily obtained through other means (flatpak, distrobox).
- Please, for your own sanity and mine, do not try and do something like change init systems. If you want to do this, feel free to make your own rootFS instead.

#### Technical details

The `/usr` overlay is implemented by a local.d service with creates the overlay directory if it doesn't exist, and mounts it. The reason we do it this way, instead of using `/etc/fstab`, is that dracut (the initramfs) will try to mount a `/usr` entry if it sees this. This actually sends you straight to a dracut rescue shell, as `/overlay` isn't mounted by this point, so dracut can't mount `/usr`.

If you want the overlay to always be read-write, you can edit the service at `/etc/local.d/usr.start` and change where it says `ro` to be `rw`.

### A better set of system utilities

Would you believe it, Xenia now has an _actual system logger_! Truly some amazing times we live in.

All joking aside, as Xenia is still very much in alpha and has been for a while, we never actually put too much thought into our base set of software beyond what we absolutely needed. This left some expected functionality out of Xenia. Granted, you can now add this yourself with the overlay, but having software like a cron daemon and a system logger is probably best included in the actual `root.img`.

We have added:

- chrony for NTP services
- cronie for a cron daemon
- sysklogd for syslog
- mlocate to index files and provide the `locate` utility

### Reworked boot logic

Previously, Xenia Linux booted by mounting the squashFS with a hard-coded path in GRUB to `(lvm/vg0-roots)`. This is not the best way to do things, as you can probably imagine. Also, we did this by editing GRUBs logic for making Linux entries! That means if GRUB found other Linux distributions, they would all become entries to Xenia Linux. While I personally think this is a feature, I don't think others would take too kindly to it.

So, I re-did the booting logic to instead look for a label, and added this to a custom template unlike what we did before. Not only does this mean dual-booting with other distros may now be possible (not tested), it means the ground-work for a new installer with multiple filesystem choices is being set.

### New installer

Up until now, all of the new features are available **today** on the unstable branch, and will be released as a part of 0.4 when we are ready.

However, we have one last announcement. [Cow](https://sandhuservices.dev/) has joined our team and has started work on a [new installer](https://gitlab.com/xenia-group/installer) for Xenia Linux.

As you might know, our current installer is pretty _terrible_. It nukes your disk with no warning, doesn't care about errors and only works on devices like `/dev/sdX` or `/dev/vdX`. Just wonderful, am I right?

Cow wanted to fix that, and has started a new installer written in Python. Here is a rough overview of what we plan to include:

- An installer interactively asks for information, or that takes a config file (written in toml) based on preference:
  - argument for ROOT_URL to specify where the `root.img` is
  - argument for the device to use
  - argument for the filesystem to use
- Proper support for block devices such as NVMe and eMMC/SD cards
- Multiple filesystem support
- A nicer experience for the user that doesn't just nuke the drive when you run it!

I wanted to highlight a specific feature, the ability for the installer to take a config file. This allows for Xenia Linux installs to be done completely autonomously. This is exciting to allow for quick deployment of Xenia on a network by serving the root image locally and installing potentially from the network to multiple workstations at once.

That about concludes all the new features added and coming soon!

## Xenia Linux wiki

One last thing to talk about before I go to bed!

Xenia Linux hasn't had much in the way of solid documentation for a while. It's been scattered across READMEs, some is horrifically outdated (even from the times when we built all our root images manually..) and some things just are not documented well at all.

To change this, I've made a new wiki [here](https://gitlab.com/groups/xenia-group/-/wikis/home). There isn't much here right now, but that will improve soon! Ideally, this will contain information on installing Xenia Linux, with both automatic and manual installations, maintaining and using the system, as well as information on the architecture of Xenia Linux.

As previously stated, there isn't much there for now. Feel free to contribute or ask for pages to be written if you want them.

## Conclusion

That about wraps up everything! Hopefully this sheds some light on where we've been - re-branding our whole project along with making some milestones with the project itself, notably now supporting Portage.

0.4 is really shaping up to be a milestone of a release. After 0.4, well - we only need a better out-of-box experience, and I say we are ready to come out of alpha.

As always, feel free to contact us with any suggestions, feedback, etc! We are always looking for places we can improve. And thank you for sticking with us and using Xenia Linux.

import type { APIContext } from "astro";
import { getCollection } from "astro:content";
import { arrangeBlogPosts } from "~/utils/arrangeBlogPosts";
import { getPostPath } from "~/utils/getPostPath";
import { parsePostDate } from "~/utils/parsePostDate";

export async function GET(ctx: APIContext) {
  const posts = await getCollection("blog");

  return new Response(
    JSON.stringify({
      posts: arrangeBlogPosts(posts, { showDrafts: false }).map(post => ({
        title: post.data.title,
        link: `${ctx.site}/blog/${getPostPath(post)}`,
        categories: post.data.categories.id,
        description: post.data.description,
        pubDate: parsePostDate(post.data.date).epoch
      }))
    })
  );
}

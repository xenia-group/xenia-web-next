import rss from "@astrojs/rss";
import type { APIContext } from "astro";
import { getCollection } from "astro:content";
import { arrangeBlogPosts } from "~/utils/arrangeBlogPosts";
import { getPostPath } from "~/utils/getPostPath";
import { parsePostDate } from "~/utils/parsePostDate";

export async function GET(ctx: APIContext) {
  const posts = await getCollection("blog");

  return rss({
    title: "Xenia Linux",
    description: "The official blog for Xenia Linux.",
    site: ctx.site!,
    items: arrangeBlogPosts(posts, { showDrafts: false }).map(post => {
      return {
        title: post.data.title,
        link: `/blog/${getPostPath(post)}`,
        categories: [post.data.categories.id],
        description: post.data.description,
        pubDate: new Date(parsePostDate(post.data.date).epoch)
      };
    })
  });
}

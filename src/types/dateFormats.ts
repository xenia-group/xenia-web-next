/**
 * Predefined list of date formats for use with spacetime library
 */
export const dateFormats = {
  full: "{day} {month} {date-ordinal} {year}, {time-24}"
} satisfies Record<string, string>;

/**
 * Union of predefined date formats for use with spacetime library
 */
export type Formats = Extract<keyof typeof dateFormats, string>;

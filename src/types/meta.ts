export interface PageMeta {
  title: string;
  description: string;
  image?: string;
  noRobots?: boolean;
  imageLarge?: boolean;
}

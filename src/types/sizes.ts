/** NOTE:
 * don't use TS Enums here, since they just cause unneccesary issues and Astro
 * does not currently properly support const enums
 */

/**
 * Sizes to use in <Picture> and getImage
 */
export const IMAGE_SIZES = {
  Meta: 1200,
  Figure: 650,
  Thumbnail: 400
} satisfies Record<string, number>;

/**
 * Media queries to use with window.matchMedia
 */
export const MEDIA_QUERIES = {
  // NOTE: these should match ./src/styles/media.css
  Large: { query: "(min-width: 992px)", size: 992 },
  Medium: { query: "(min-width: 768px)", size: 768 },
  Small: { query: "(min-width: 512px)", size: 512 }
} satisfies Record<string, { query: string; size: number }>;

/**
 * Content widths, should match what's commonly used in each of the layouts
 */
export const WIDTHS = {
  Blog: { value: "800px", size: 800 },
  Landing: { value: "1200px", size: 1200 }
} satisfies Record<string, { value: string; size: number }>;

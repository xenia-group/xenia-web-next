import type { CollectionEntry } from "astro:content";
import { parsePostDate } from "./parsePostDate";

interface PostFilters {
  category?: string;
  showDrafts?: boolean;
  // TODO: pagination
  // page?: number
}

export function arrangeBlogPosts(
  posts: CollectionEntry<"blog">[],
  filters: PostFilters = {}
) {
  let filtered: CollectionEntry<"blog">[] | null = null;

  // potentially filter by category
  if (filters.category) {
    filtered = posts.filter(
      post => post.data.categories.id == filters.category
    );
  }

  // potentially remove drafts
  if (filters.showDrafts !== true) {
    filtered = (filtered || posts).filter(post => !post.data.draft);
  }

  const sorted = (filtered || posts)
    // map to tuple with post data and date as timestamp
    .map(post => [post, parsePostDate(post.data.date).epoch] as const)
    // sort by timestamp
    .sort((a, b) => b[1] - a[1])
    // map again to remove the timestamp
    .map(([post]) => post);

  /** TODO: pagination
   * might reorder this function a little to first to implement caching for
   * the full list of sorted posts and then filter and slice that according
   * to the filters.
   *
   * Might also be nice to have caching in parsePostDate to remove extra loops
   * in this function.
   */

  return sorted;
}

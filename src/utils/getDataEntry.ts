/**
 * Wraps Astro's `getEntry` but with a non-nullable result
 * @throws if entry not found
 */

import { getEntry } from "astro:content";

export async function getDataEntry<TCollection extends "authors" | "category">(
  collection: TCollection,
  id: string
) {
  const entry = await getEntry(collection, id);
  if (!entry) {
    throw new Error(`No entry '${id}' in '${collection}'`);
  }

  return entry;
}

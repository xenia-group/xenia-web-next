import { getEntry } from "astro:content";

export async function getPostByPath(path: string) {
  const segments = path.split("/");
  const yearIdx = segments.findIndex(v => /^\d{4,}$/.test(v));
  const id = segments.slice(yearIdx + 3).join("/");
  const post = await getEntry("blog", id);
  return post;
}

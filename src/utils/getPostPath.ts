import type { CollectionEntry } from "astro:content";
import { parsePostDate } from "./parsePostDate";

export function getPostPath(post: CollectionEntry<"blog">) {
  const date = parsePostDate(post.data.date);
  return `${post.data.categories.id}/${date.s.format(
    "{year}/{iso-month}/{date-pad}"
  )}/${post.id}`;
}

import spacetime from "spacetime";
import { dateFormats } from "~/types/dateFormats";

function parseOffset(raw: string) {
  const negative = raw.startsWith("-");
  const hoursPad = raw.slice(1, 3);
  const hours = (negative ? -1 : 1) * Number(hoursPad);
  const minutesPad = raw.slice(3);
  const minutes = (negative ? -1 : 1) * Number(minutesPad);
  const str = `${negative ? "-" : "+"}${hoursPad}:${minutesPad}`;
  return { hours, minutes, str };
}

export function parsePostDate(dateStr: string) {
  const match = dateStr.match(
    /^(?<year>\d{4})-(?<month>\d{2})-(?<day>\d{2}) (?<hour>\d{2}):(?<minute>\d{2}):(?<second>\d{2}) (?<offset>\+\d{4}|-\d{4})$/
  );

  if (!match) {
    throw new Error(
      `Could not parse date: '${dateStr}'\nDate should match format: YYYY-mm-DD HH:MM:ss Z`
    );
  }

  const raw = match.groups!;
  const offset = parseOffset(raw.offset);
  const timeStr = `${raw.year}-${raw.month}-${raw.day}T${raw.hour}:${raw.minute}:${raw.second}.000`;
  const s = spacetime(timeStr, "utc");
  const formatted = `${s.format(dateFormats.full)} ${offset.str}`;
  const epoch = s
    .subtract(offset.hours, "hour")
    .subtract(offset.minutes, "minute").epoch;

  return {
    /**
     * Parsed date formatted as readable string
     */
    formatted,
    /**
     * Parsed date formatted as unix timestamp in milliseconds
     */
    epoch,
    /**
     * Parsed date formatted as datetime with offset
     */
    datetime: `${timeStr}${offset.str}`,
    /**
     * Spacetime instance used for formatting on the server.
     * Note: This instance is incorrectly set to utc, timezone offset is applied correctly!
     */
    s
  };
}
